# mobility



## Requirement

For building and running the application you need:

- JDK 11
- MySql


## Database
In this application, we use MySQL database. you should create a database called **leasing** . A user called admin should be created in MySQL.
this is application.properties file :

```
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/leasing
spring.datasource.username=admin
spring.datasource.password=admin
spring.jpa.hibernate.ddl-auto=validate

spring.flyway.locations=classpath:db/migration/mysql
```

## How it works
The application uses Spring Boot (Web, data-jpa, validation).

- Use LomBok to create getter, setter and constructors.
- Use Flyway as version control for database.

And the code is organized as this:

- api is the rest controller layer
- service is the business services
- repository is for working with database
- model contains all entities


